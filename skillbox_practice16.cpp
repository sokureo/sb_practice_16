#include <iostream>
#include <time.h>


int main()
{	
	const int N = 3;
	int array[N][N];

	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			array[i][j] = i + j;
		}
	}

	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			std::cout << array[i][j] << ' ';
		}
		std::cout << '\n';
	}

	struct tm buf;
	time_t t = time(NULL);
	localtime_s(&buf, &t);
	int day = buf.tm_mday;
	
	int indx = day % N;
	int sum = 0;

	for (int j = 0; j < N; j++)
	{
		sum += array[indx][j];
	}
	std::cout << sum;
}

